package com.itzli.pruebalistas;

/**
 * Created by itzli on 21/10/2016.
 */

public class FrutasVerduras {

    public int icon;
    public String title;
    public FrutasVerduras(){
        super();
    }

    public FrutasVerduras(int icon, String title){
        super();
        this.icon = icon;  //Lo que recibimos...
        this.title = title;//nos lo asignamos
    }
}
